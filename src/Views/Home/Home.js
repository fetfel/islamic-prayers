import React, { useState, useEffect } from 'react';
import { getPrayerTiming } from '../../Api/prayerTimeAPI';
import DayPrayer from "../../Components/DayPrayer";
import {TIMING_NAMES} from '../../Utils/Variables';
// import './home.css';

export default function Home() {
  const [pTimeCalendar, setPrayerTimeCalendar] = useState({});
  const [pTimingAllData, setPrayerTimingAllData] = useState({});
  const [pTimingMinimized, setPrayerTimingMinimized] = useState([])

  useEffect(() => {
    const ts = Math.round(new Date().getTime() / 1000)
    getPrayerTiming(ts)
      .then((r) => {
        setPrayerTimingAllData(r.data)
        const prayerTimes = TIMING_NAMES.map((item) => r.data.timings[item]);
        setPrayerTimingMinimized(prayerTimes);
        console.log({ prayerTimes });
      }).catch((e) => {
        window.alert('failed to load data');
      })
  }, [])
  return (
    <div>
      {/* {JSON.stringify(pTimeCalendar)} */}
      <DayPrayer
        pTiming={pTimingMinimized}
      />
    </div>
  )
}
