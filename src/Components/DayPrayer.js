import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { TIMING_NAMES } from "../Utils/Variables";

function DayPrayer(props) {
  const { pTiming = [] } = props;
  const [currentTs, setCurrentTs] = useState(new Date());
  // const [pTimingConvertedToTs, setPTimeConverted] = useState([]);
  useEffect(() => {
    const it = setInterval(() => {
      setCurrentTs(new Date());
    }, 1000);
    return () => {
      clearInterval(it)
    }
  }, []);

  return (
    <div>
      <h4> {currentTs.toLocaleTimeString('fr', {hour: '2-digit', minute: '2-digit', second: '2-digit'})} </h4>
      {pTiming.map((i, index) => <span style={{ margin: '10px 20px' }}>{TIMING_NAMES[index]}: {i}</span>)}
    </div >
  )
}

DayPrayer.propTypes = {
  pTiming: PropTypes.objectOf({
    timings: PropTypes.shape({}),
    date: PropTypes.shape({})
  })
}
export default DayPrayer;