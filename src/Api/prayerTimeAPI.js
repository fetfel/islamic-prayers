export const getPrayerTimeCalendar = (lat = 51.508515, lng = -0.1254872, month = 4, year = 2017) => fetch(
    `http://api.aladhan.com/v1/calendar?latitude=${lat}&longitude=${lng}&method=2&month=${month}&year=${year}/`
).then((r) => r.json());

// timing of date based on timestamp and location
export const getPrayerTiming = (ts = 1398332113, lat = 36.7598765, lng = 10.08889925) => fetch(
    `http://api.aladhan.com/v1/timings/${ts}?latitude=${lat}&longitude=${lng}&method=2`
).then((r) => r.json());


//http://api.aladhan.com/v1/timings/1398332113?latitude=51.508515&longitude=-0.1254872&method=2